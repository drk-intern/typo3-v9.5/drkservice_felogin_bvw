<?php
namespace Oranto\DrkserviceFeloginBvw\Controller;

use TYPO3\CMS\Core\Log\LogManager;
use TYPO3\CMS\Extbase\Mvc\Exception\StopActionException;
use TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException;
use TYPO3\CMS\Core\Cache\CacheManager;
use Oranto\DrkserviceFeloginBvw\Utility\FrontendUserUtility;
use Oranto\DrkserviceFeloginBvw\Utility\PagesUtility;
use Oranto\DrkserviceFeloginBvw\Utility\ZeichenUtility;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Core\Database\ConnectionPool;
use Oranto\DrkserviceFeloginBvw\Domain\Repository\FeloginRepository;

/***
 *
 * This file is part of the "DRK Service - BVW- FE Login" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Ting Cao <Ting.Cao@Oranto.de>, Oranto GmbH
 *
 ***/
/**
 * FeloginController
 */
class FeloginController extends ActionController {
	/**
	 * @var \SoapClient
	 */
	private $soapClient = null;

    /** @var $connectionPool  TYPO3\CMS\Core\Database\ConnectionPool */
	private $connectionPool;

    /** @var $logger \TYPO3\CMS\Core\Log\Logger */
    private $logger;

	const METHOD_LOGIN = 'typo3Login'; //typo3Login
	const METHOD_GETDATA = 'getTYPO3DataByTicket'; //getTYPO3DataByTicket
	const METHOD_GETUSERGROUPS = 'getTYPO3Usergroups'; // getTYPO3Usergroups

	const PREFIX_USERGROUP = 'BVW';

	private $debug = false;
    private $use_debugdata = false;
	private $logging = false;

    /**
     * orderFormRepository
     *
     * @var \Oranto\DrkserviceFeloginBvw\\Domain\Repository\FeloginRepository
     */
    protected $feloginRepository = NULL;

    /**
     * @param FeloginRepository $repository
     * @return void
     */
    public function injectFeloginRepository(FeloginRepository $repository)
    {
        $this->feloginRepository = $repository;
    }

    /**
     * Init
     *
     * @return void
     */
    protected function initializeAction()
    {
        parent::initializeAction();

        /** @var $logger \TYPO3\CMS\Core\Log\Logger */
        $this->logger = GeneralUtility::makeInstance(LogManager::class)->getLogger(__CLASS__);

        /** @var connectionPool  TYPO3\CMS\Core\Database\ConnectionPool */
        $this->connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);

    }

	/**
     * action loginForm
     *
     * @return void
     */
    public function loginFormAction() {
		$feUserId = $GLOBALS['TSFE']->fe_user->user['uid'] ?? null;

		if (!empty($feUserId)) {
            $feUserRow = $this->feloginRepository->getFeUser($feUserId);
			if (isset($feUserRow) && count($feUserRow) > 0) {
				$this->view->assign('feUserRow', $feUserRow);
			}
		}
    }

	/**
	 * action login
	 *
	 * @return void
	 * @throws StopActionException
	 * @throws UnsupportedRequestTypeException
	 */
	public function loginAction() {

		$this->_initSoapClient();

		if ($this->soapClient == null) {
			return;
		}

		$wsdl = $this->settings['flexform']['wsdl'];
		$ticket_gp = GeneralUtility::_GP('ticket');
		$username = GeneralUtility::_GP('user');
		$password = GeneralUtility::_GP('pass');


		if ((empty($username) OR empty($password)) AND empty($ticket_gp)) {
            $this->addFlashMessage('Bitte mit Benutzername und Passwort anmelden!', 'Fehler ', AbstractMessage::ERROR);
            return;
        }
		elseif ((empty($username) AND empty($password)) AND empty($ticket_gp)) {
            $this->addFlashMessage('Fehler in der Anmeldung!', 'Fehler ', AbstractMessage::ERROR);
            return;
        }

		#################################
		### Nutzerdaten aus BVW holen ###
		#################################

		if (!$this->debug && !$this->use_debugdata) {
			// Ticket über SOAP holen wenn nicht vorhanden
			if (isset($ticket_gp) && strlen($ticket_gp) > 1) {
				$ticket = $ticket_gp;
			} else {
				$ticket = $this->_login(self::METHOD_LOGIN, $username, $password);
			}

			if (!$ticket) {
                if ($this->debug) {
                    $this->addFlashMessage('Ticket nicht vorhanden', 'SOAP ' . $wsdl, AbstractMessage::ERROR);
                }
                else {
                    $this->addFlashMessage('Ticket nicht vorhanden', 'Fehler ', AbstractMessage::ERROR);
                }

				if ($this->logging) $this->logger->debug('Ticket nicht vorhanden', ['wsdl' => $wsdl, 'username' => $username]);
				return;
			}
		}

		if ($this->debug  && $this->use_debugdata) {
			$responseArray = $this->_getDataDebug();
		} else {
			$responseArray = $this->_getDataByTicket(self::METHOD_GETDATA, $ticket);
		}

		if (!$responseArray) {
			if ($this->debug) {
                $this->addFlashMessage('Nutzerdaten nicht vorhanden', 'SOAP ' . $wsdl, AbstractMessage::ERROR);
            }
			else {
                $this->addFlashMessage('Nutzerdaten nicht vorhanden', 'Fehler' , AbstractMessage::ERROR);
            }
            if ($this->logging) $this->logger->debug('Nutzerdaten nicht vorhanden', ['wsdl' => $wsdl, 'username' => $username]);

			return;
		}

		##########################################
		### Login gültig, fe_users & fe_groups ###
		##########################################

		// Datensatzsammlung für das Plugin
		$storagePageIds = $this->configurationManager->getContentObject()->data['pages'];
		$storagePageRecursive = $this->configurationManager->getContentObject()->data['recursive'];
		$parameter['storagePageRecursiveArray'] = PagesUtility::getStoragePageRecursiveArray($storagePageIds, $storagePageRecursive, TRUE);

		$feUserId = $this->_updateUser($responseArray, $username, $password);

		if (isset($feUserId) && $feUserId > 0) {
			// mit cookie einloggen
			FrontendUserUtility::loginUser($feUserId);

			if ($this->settings['flexform']['redirectPid']) {
				$pageUid = $this->settings['flexform']['redirectPid'];
				$uriBuilder = $this->uriBuilder;
				$uri = $uriBuilder
					->setTargetPageUid($pageUid)
					->build();
				// clear cache of the redirect page
                GeneralUtility::makeInstance(CacheManager::class)
                    ->flushCachesInGroupByTags('pages', [ 'pageId_'.$pageUid ]);

				$this->redirectToUri($uri, $delay=0, $statusCode=303);
			}
		} else {
			$this->addFlashMessage('Nutzer konnte nicht angemeldet werden.', 'SOAP ' . $wsdl, AbstractMessage::ERROR);
			if ($this->logging) $this->logger->error('Nutzer konnte nicht angemeldet werden.', ['wsdl' => $wsdl, 'username' => $username]);
			return;
		}
	}

	private function logoutAction()
    {

    }

	/**
	 */
	private function _initSoapClient() {
		$wsdl = $this->settings['flexform']['wsdl'];

		try {
            $soapOptions = [
                'stream_context' => stream_context_create(
                    [
                        'http' => [
                            'user_agent' => 'PHPSoapClient'
                        ]
                    ]
                ),
                'soap_version' => SOAP_1_2,
                'encoding' => 'UTF-8',
                'trace' => true
            ];

			$this->soapClient = new \SoapClient($wsdl, $soapOptions);
		} catch (\Error $error) {
			$this->addFlashMessage($error->getMessage(), 'SOAP ' . $wsdl, AbstractMessage::ERROR);
			if ($this->logging) $this->logger->error($error->getMessage(), ['wsdl' => $wsdl]);
			return;
		} catch (\Exception $exception) {
			$this->addFlashMessage($exception->getMessage(), 'SOAP ' . $wsdl, AbstractMessage::ERROR);
			if ($this->logging) $this->logger->error($exception->getMessage(), ['wsdl' => $wsdl]);
			return;
		}
	}

	/**
	 * @param $method
	 * @param $username
	 * @param $password
	 * @return mixed
	 */
	private function _login($method, $username, $password) {
		try {
			$response = $this->soapClient->$method($username, $password);
		} catch (\Exception $exception) {
			$this->addFlashMessage($exception->getMessage(), 'SOAP ' . $this->settings['flexform']['wsdl'], AbstractMessage::ERROR);
			if ($this->logging) $this->logger->debug($exception->getMessage(), ['username' => $username]);
			return null;
		}

		$responseArray = (array)$response;

		if ($responseArray['error'] == true) {
			$this->addFlashMessage($responseArray["message"] . ': ' . $username, 'SOAP ' . $this->settings['flexform']['wsdl'], AbstractMessage::ERROR);
			if ($this->logging) $this->logger->debug($responseArray["message"], ['username' => $username]);
			return null;
		}

		return $responseArray['ticket'];
	}

	/**
	 * usergroups in assozietes array umwandeln
	 * @param $method
	 * @return array
	 */
	private function _getUsergroups($method) {
		$wsdl = $this->settings['flexform']['wsdl'];

		try {
			$groups = $this->soapClient->$method();
		} catch (\Exception $exception) {
			$this->addFlashMessage($exception->getMessage(), 'SOAP ' . $wsdl, AbstractMessage::ERROR);
			if ($this->logging) $this->logger->debug($exception->getMessage(), ['wsdl' => $wsdl]);
			return null;
		}

		$groupsArray = (array)$groups;
		$usergroupsArray = $this->unserialize($groupsArray['feusergroups']);
		$usergroups = array();
		foreach ($usergroupsArray as $sergroupid => $usergrouname) {
			$usergroups[$sergroupid] = $usergrouname;
		}

		return $usergroups;
	}

	/**
	 * @param $method
	 * @param $ticket
	 * @return array|null
	 */
	private function _getDataByTicket($method, $ticket) {
		$responseArray = null;

		$wsdl = $this->settings['flexform']['wsdl'];

		try {
			$response = $this->soapClient->$method($ticket);
		} catch (\Exception $exception) {
			$this->addFlashMessage($exception->getMessage(), 'SOAP ' . $wsdl, AbstractMessage::ERROR);
			if ($this->logging) $this->logger->debug($exception->getMessage(), ['wsdl' => $wsdl, 'ticket' => $ticket]);
			return null;
		}

		$responseArray = (array)$response;

		if ($responseArray["error"] == true) {
			$this->addFlashMessage($responseArray['message'], 'SOAP ' . $wsdl, AbstractMessage::ERROR);
			if ($this->logging) $this->logger->debug($responseArray['message'], ['wsdl' => $wsdl, 'ticket' => $ticket]);
			return null;
		}

		$responseArray['ticket'] = $ticket;
		$responseArray['user'] = unserialize($responseArray['user']);
		$responseArray['usergroups'] = $this->_getUsergroups(self::METHOD_GETUSERGROUPS);

		return $responseArray;
	}

	/**
	 * @return array
	 */
	private function _getDataDebug() {
		$responseArray = array (
			'error' => false,
			'message' => '',
			'person_id' => 32380,
			'user' =>
				array (
					0 =>
						array (
							'user_id' => 35368,
							'role' => 30,
							'isActive' => 1,
							'org_code' => '2506',
							'org_name' => 'Testkreisverband',
							'group' => '3,14',
						),
					1 =>
						array (
							'user_id' => 35369,
							'role' => 10,
							'isActive' => 1,
							'org_id' => 3014,
							'org_code' => '2501',
							'org_name' => 'Musterstadt e.V.',
							'group' => '3',
						),
				),
			'isActive' => 1,
			'prefix' => 'Frau',
			'title' => 'kein',
			'firstname' => 'Grit',
			'lastname' => 'WDBDoppeltangelegt',
			'office_street' => '',
			'office_zipcode' => '',
			'office_city' => 'Musterstadt',
			'office_phone' => '',
			'office_fax' => '',
			'office_email' => '',
		);

		$responseArray['usergroups'] = array (
			1 => 'Kreisgeschäftsführer',
			2 => 'Schwesternschaft',
			3 => 'Sonstige',
			4 => 'Bayern',
			5 => 'Auslandshilfe',
			6 => 'Nordrhein',
			7 => 'RD-NiSa',
			8 => 'Strategieverantwortliche',
			9 => 'LV-Leiter KOMA',
			10 => 'WB-Contentmanagement',
			11 => 'Hausnotruf-AG',
			12 => 'WB-Datenpfleger',
			13 => 'Multiplikator',
			14 => 'DRKS Verlag',
			15 => 'CRM-Beirat',
		);

		return $responseArray;
	}

	/**
	 * @param $responseArray
	 */
	private function _updateUsergroups ($responseArray) {

		$pid = $this->_getPid();
		$queryBuilder = $this->connectionPool->getQueryBuilderForTable('fe_groups');

		foreach ($responseArray['usergroups'] as $id => $group) {
			$titelPrefix  = self::PREFIX_USERGROUP . '-' . $id . '-';
			$titel = $titelPrefix . $group;

			$groupDataArray = [
				'pid' => $pid,
				'title' => $titel,
				'description' => 'Group was created by DRK-Service BVw Webservice',
				'tstamp' => time(),
				'crdate' => time()
			];

            $queryResult = $queryBuilder->select('uid')
                ->from('fe_groups')
                ->where(
                    $queryBuilder->expr()->like('title', $queryBuilder->createNamedParameter($titelPrefix.'%'))
                )
                ->setMaxResults(1)
                ->execute();

            $groupExist = $queryResult->fetch();

			//$groupExist = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
			//	'*',
			//	'fe_groups',
			//	"title like '%" . $titelPrefix . "%'",
            //    "deleted = 0"
			//);


			if ($groupExist == false) {
				$this->_createUsergroup($groupDataArray);
			} else {

                $this->connectionPool
                    ->getConnectionForTable('fe_groups')
                    ->update(
                        'fe_users',
                        $groupDataArray,
                        [ 'uid' => (int)$groupExist['uid']]
                    );

				//$GLOBALS['TYPO3_DB']->exec_UPDATEquery('fe_groups', 'uid = ' . $groupExist['uid'], $groupDataArray);
			}
		}
	}

	/**
	 * neuer fe_groups Datensatz
	 *
	 * @param $groupDataArray
	 * @return int
	 */
	private function _createUsergroup($groupDataArray) {

        $this->connectionPool
            ->getConnectionForTable('fe_groups')
            ->insert(
                'fe_groups',
                $groupDataArray
            );

        $newFeGroupUid = (int)$this->connectionPool->getConnectionForTable('fe_groups')->lastInsertId('fe_groups');

		//$res = $GLOBALS['TYPO3_DB']->exec_INSERTquery('fe_groups', $groupDataArray);

		//if ($res == TRUE) {
		//	$newFeGroupUid = $GLOBALS['TYPO3_DB']->sql_insert_id();
		//}

		/** @var integer $newFeGroupUid */
		return $newFeGroupUid;
	}

	/**
	 * @param $responseArray
	 * @param $username
	 * @param $password
	 * @return int
	 */
	private function _updateUser ($responseArray, $username, $password) {

		// Gruppe updaten evtl. neue Gruppe anlegen
		$this->_updateUsergroups($responseArray);

		$pid = $this->_getPid();
		$usergroup = $this->_getUsergroupUid($responseArray);

		$userDataArray = [
			'bvw_person_id' => $responseArray['person_id'],
			'sso_ticket' => strlen($responseArray['ticket'])>1 ? $responseArray['ticket'] : '',
			'username'  => $responseArray['username'] ,
			'password' => !empty($password) ? ZeichenUtility::passwort2passwortRsa($password) : $responseArray['ticket'], // fake password if only login ticket was given
			'name'=> $responseArray['firstname'] . ' ' . $responseArray['lastname'],
			'first_name'=> $responseArray['firstname'],
			'last_name'=> $responseArray['lastname'],
			'email' => $responseArray['office_email'],
			'title' => !empty($responseArray['prefix']) ? $responseArray['prefix'] : 'Herr' ,
			'usergroup' => implode(',', $usergroup),
            'description' => 'User was created by DRK-Service BVw Webservice',
			'pid' => $pid,
			'tstamp' => time(),
			'crdate' => time()
		];

        $queryBuilder = $this->connectionPool->getQueryBuilderForTable('fe_users');
        $queryResult= $queryBuilder->select('uid')
            ->from('fe_users')
            ->where(
                $queryBuilder->expr()->eq('bvw_person_id', $queryBuilder->createNamedParameter($responseArray['person_id'], \PDO::PARAM_INT))
            )
            ->setMaxResults(1)
            ->execute();

        $userExist = $queryResult->fetch();

/*		$userExist = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
			'*',
			'fe_users',
			"bvw_user_id=" . $responseArray['person_id'],
            "deleted = 0"
		);*/

		if ($userExist == false) {
			$feUserId = $this->_createFeUser($userDataArray);
		} else {
            $this->connectionPool
                ->getConnectionForTable('fe_users')
                ->update(
                    'fe_users',
                    $userDataArray,
                    [ 'uid' => $userExist['uid'] ]
                );

		    //$GLOBALS['TYPO3_DB']->exec_UPDATEquery('fe_users', 'bvw_user_id = ' . $responseArray['person_id'], $userDataArray);
			$feUserId = $userExist['uid'];
		}

		return $feUserId;
	}

	/**
	 * neuer fe_users Datensatz
	 *
	 * @param $userDataArray
	 * @return int
	 */
	private function _createFeUser($userDataArray) {

        $this->connectionPool
            ->getConnectionForTable('fe_users')
            ->insert(
            'fe_users',
            $userDataArray
            );
        $newFeUserUid = (int)$this->connectionPool->getConnectionForTable('fe_users')->lastInsertId('fe_users');

		/** @var integer $newFeUserUid */
		return $newFeUserUid;
	}

	/**
	 * @param $responseArray
	 * @return array
	 */
	private function _getUsergroupUid($responseArray) {

	    $usergroupUids = [];
		$groupIds = [];

		$usergroups = $responseArray['usergroups'];

		foreach ($responseArray['user'] as $verbanduser) {
            $groups = [];
			if ($verbanduser['isActive'] == 1 && $verbanduser['group'] != '') {
                $groups = explode( ',' , $verbanduser['group']);
                $groupIds = array_merge($groupIds, $groups);
			}
		}

		// we only need distinct groupids
        $groupIds = array_flip(array_flip($groupIds));

        $queryBuilder = $this->connectionPool->getQueryBuilderForTable('fe_groups');
		foreach ($groupIds as $id) {
			$group = $usergroups[$id];
			$title = self::PREFIX_USERGROUP . '-' . $id . '-' . $group;

            $queryResult= $queryBuilder->select('uid')
                ->from('fe_groups')
                ->setMaxResults(1)
                ->where(
                    $queryBuilder->expr()->eq('title', $queryBuilder->createNamedParameter($title))
                )
                ->execute();

            $groupExist = $queryResult->fetch();

			//$res = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
			//	'*',
		    //  'fe_groups',
			//	"title='" . $title . "'"
			//);
			if (isset($groupExist['uid']) && (int)$groupExist['uid'] > 0) {
				$usergroupUids[] = $groupExist['uid'];
			}
		}
		return $usergroupUids;
	}

	/**
	 * @return int
	 */
	private function _getPid(): int
	{
		$pid = $this->settings['flexform']['storagePid'];
		if (is_numeric($pid) && $pid > 0) {
			return $pid;
		} else {
			return 0;
		}
	}

    /**
     * @param string $sSerializedStr
     * @return mixed|null|string
     */
    public static function unserialize($sSerializedStr)
    {
        // 'serialized' with json_encode()
        if (null === ($sOut = @json_decode($sSerializedStr, true))) {
            $sOut = $sSerializedStr;

            // serialized with serialize()
            if (1 === preg_match('/^a:\d+:{.*}$/s', $sSerializedStr)) {
                $sOut = preg_replace_callback(

                    '!(?<=^|;)s:(\d+)(?=:"(.*?)";(?:}|a:|s:|b:|d:|i:|o:|N;))!s',
                    static function ($aMatches) {
                        $iLen = mb_strlen($aMatches[2], '8bit');
                        return "s:{$iLen}";
                    },
                    $sSerializedStr
                );

                $sOut = @unserialize($sOut);
            }
        }
        return $sOut;
    }
}

<?php

namespace Oranto\DrkserviceFeloginBvw\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 André Gyoengyoesi <a.gyoengyoesi@drkservice.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkGeneral\Domain\Repository\AbstractDrkRepository;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;


/**
 * FeloginRepository
 */
class FeloginRepository extends AbstractDrkRepository
{

    /**
     * @param $feUserId
     * @return false|mixed[]
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    public function getFeUser ($feUserId = 0) {

        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);

        /** @var \TYPO3\CMS\Core\Database\Query\QueryBuilder $queryBuilder */
        $queryBuilder = $connectionPool->getQueryBuilderForTable('fe_users');

        try {
            return $queryBuilder
                ->select('*')
                ->from('fe_users')
                ->where($queryBuilder->expr()->eq('uid', $feUserId))
                ->execute()
                ->fetchAssociative();


        } catch (\Exception $exception) {
            $this->error = ['Fehler' => $exception->getMessage()];
            return false;
        }
    }

}

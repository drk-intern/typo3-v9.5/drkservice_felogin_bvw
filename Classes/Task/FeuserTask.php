<?php
namespace Oranto\DrkserviceFeloginBvw\Task;

use TYPO3\CMS\Scheduler\Task\AbstractTask;

/***
 *
 * This file is part of the "DRKService.de und DRK-Einkaufsportal.de und DRK-Mitgliederbrief" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Heinrich Pegelow <H.Pegelow@oranto.de>, Oranto GmbH
 *           Ting Cao <Ting.Cao@Oranto.de>, Oranto GmbH
 *           Robert Kärner <R.Kaerner@oranto.de>, Oranto GmbH
 *
 ***/

/**
 * Die Erweiterungsklasse um die diverse Aufgaben an der Datenbank auszufuehren
 */
class FeuserTask extends AbstractTask {

	/**
	 * Function executed from scheduler
	 */
	public function execute()
	{
		// $GLOBALS['TYPO3_DB']->exec_SELECTgetRows($select_fields, $from_table, $where_clause, $groupBy = '', $orderBy = '', $limit = '', $uidIndexField = '');

		$jetzt = time();
		// 6 Monate
		$sechsMonate = intval(60*60*24*(365+0.25)/2);
		$sechsMonateTstamp = $jetzt - $sechsMonate;

		// $feusers = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('*','fe_users','lastlogin<'.$sechsMonateTstamp);
		$GLOBALS['TYPO3_DB']->exec_UPDATEquery('fe_users', 'lastlogin<'.$sechsMonateTstamp, ['deleted' => 1]);

		return TRUE;
	}
}

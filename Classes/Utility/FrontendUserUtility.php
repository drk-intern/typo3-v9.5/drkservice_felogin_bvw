<?php
namespace Oranto\DrkserviceFeloginBvw\Utility;

class FrontendUserUtility {

    /**
     * @var
     */
    protected $context;

	/**
	 * loggt einen User ein
	 *
	 * @return void
	 */
	public static function loginUser($feUserId) {
		$feuserArray = ['uid' => $feUserId];

		// 0 for session cookie, 1 for a permanent cookie
		// $GLOBALS['TSFE']->fe_user->is_permanent = $feuser->getRememberMe();
		// $GLOBALS['TSFE']->fe_user->is_permanent = 0; // bei Ablaufdatum des Cookies steht "Sitzungsende" (Firefox) und "N/A" (Chrome), das führt irgendwie (!) dazu, dass die Sitzung von PHP (Mittwald?) beendet wird
		$GLOBALS['TSFE']->fe_user->is_permanent = 1;
		$GLOBALS['TSFE']->fe_user->sessionTimeout = (int)60*60*24*7; // keine Ahuung ob das was bringt

		$GLOBALS['TSFE']->fe_user->checkPid = 0;
		$GLOBALS['TSFE']->fe_user->forceSetCookie = TRUE; // reicht nicht immer
		$GLOBALS['TSFE']->fe_user->dontSetCookie = FALSE; // hat das Problem, dass Cookies nicht immer (!) geschrieben wurden letztlich gelöst
		$GLOBALS['TSFE']->fe_user->start(); // soll man aus FailSafeGründen machen

		$GLOBALS['TSFE']->fe_user->createUserSession($feuserArray);
		$GLOBALS['TSFE']->fe_user->user = $GLOBALS['TSFE']->fe_user->fetchUserSession();
		$GLOBALS['TSFE']->fe_user->fetchGroupData();

        //$this->context = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Context\Context::class);
        //$this->context->getPropertyFromAspect('frontend.user', 'isLoggedIn');

		$GLOBALS['TSFE']->loginUser = true;

		$GLOBALS['TSFE']->fe_user->setAndSaveSessionData('dummy', TRUE);
	}

}

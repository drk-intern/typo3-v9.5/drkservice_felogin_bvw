<?php
namespace Oranto\DrkserviceFeloginBvw\Utility;

use TYPO3\CMS\Core\Database\QueryGenerator;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/**
 * Class PagesUtility
 */
class PagesUtility {
	/**
	 * zu einer gegebenen Seite alle Unterseiten finden
	 * geht x ($tiefe) Ebenen tief
	 * liefert ein Array ODER eine Komagetrennte Liste zurueck
	 *
	 * @param integer $parent
	 * @param integer $tiefe
	 * @param boolean $asArray
	 * @return integer|array $treePids
	 */
	public static function getTreePids($parent = 0, $tiefe = 0, $asArray = true) {
		$queryGenerator = GeneralUtility::makeInstance(QueryGenerator::class);

		$childPids = $queryGenerator->getTreeList($parent, $tiefe, 0, 1); //Will be a string like 1,2,3

		// jetzt muss ich PARENT noch aus der Liste loeschen
		// ein Array draus machen, weil ich da besser loeschen kann
		$childPidsArray = explode(',', $childPids);
		$childPidsArrayParentKey = array_search($parent, $childPidsArray);
		unset($childPidsArray[$childPidsArrayParentKey]);

		if ($asArray == TRUE) {
			return $childPidsArray;
		} else {
			return implode(',', $childPidsArray);
		}
	}

    /**
     * zu einem gegebenen String mit IDs von Seiten UND einen Parameter zu jeder dieser ID die rekursiven (je nach Parameter) Seiten-IDs ermitteln
     * geht x ($tiefe) Ebenen tief
     * liefert ein Array ODER eine kommagetrennte Liste zurueck
     *
     * @param string $pids als kommagetrennter String mit Seiten-IDs
     * @param integer $tiefe
     * @param boolean|NULL $asArray
     * @return integer|array $treePids
     */
    public static function getStoragePageRecursiveArray($pids, $tiefe = 0, $asArray = TRUE) {
        $pidsArray = explode(',', $pids);
        $storagePageRecursiveArray = array();
        foreach ($pidsArray as $pid) {
            // erstmal die Elternseite wegspeichern
            $storagePageRecursiveArray[] = $pid;
            // dann die Kinder seiten wegspeichern
            $treePidsRecursive = self::getTreePids($pid, $tiefe, TRUE);
            if (is_array($treePidsRecursive)) {
                foreach ($treePidsRecursive as $key => $value) {
                    array_push($storagePageRecursiveArray, $value);
                }
            } else {
                $storagePageRecursiveArray[] = $treePidsRecursive;
            }
        }

        if ($asArray == TRUE) {
            return $storagePageRecursiveArray;
        } else {
            return implode(',', $storagePageRecursiveArray);
        }
    }
}
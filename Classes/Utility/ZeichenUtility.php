<?php
namespace Oranto\DrkserviceFeloginBvw\Utility;

use TYPO3\CMS\Core\Crypto\PasswordHashing\SaltedPasswordsUtility;
use TYPO3\CMS\Core\Crypto\PasswordHashing\PasswordHashFactory;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

/**
 * Zeichen und Strings - alles was damit zu tun hat...
 */

class ZeichenUtility {

	/**
	 * nicht erlaubte Zeichen entfernen
	 * @param $parameter
	 * @return array|mixed
	 */
	public static function paramCheck($parameter) {
		$search = ['%', '!', '<', '>', '\'', '"'];
		$replace = '';
		return str_replace($search, $replace, $parameter);
	}

	/**
	 * Ein gesalzendes Passwort erzeugen
	 *
	 * @param string $passwort
	 * @internal param string $string
	 * @return string $saltedPassword
	 */
	public static function passwort2passwortRsa($passwort) {
		$saltedPassword = '';
		if (ExtensionManagementUtility::isLoaded('saltedpasswords')) {
			if (SaltedPasswordsUtility::isUsageEnabled('FE')) {
				$objSalt = PasswordHashFactory::getSaltingInstance(NULL);
				if (is_object($objSalt)) {
					$saltedPassword = $objSalt->getHashedPassword($passwort);
				}
			}
		}
		return $saltedPassword;
	}
}

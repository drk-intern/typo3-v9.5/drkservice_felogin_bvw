<?php

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'DrkserviceFeloginBvw',
    'Felogin',
    'DRK Service BVW FE Login'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('drkservice_felogin_bvw', 'Configuration/TypoScript', 'DRK Service - BVW- FE Login');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_drkservicefeloginbvw_domain_model_felogin', 'EXT:drkservice_felogin_bvw/Resources/Private/Language/locallang_csh_tx_drkservicefeloginbvw_domain_model_felogin.xlf');

/***********
 * Flexforms
 */
$extensionName = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase('drkservice_felogin_bvw');

$pluginSignature = strtolower($extensionName) . '_' .strtolower('Felogin') ;

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'select_key';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $pluginSignature,
    'FILE:EXT:drkservice_felogin_bvw/Configuration/FlexForms/FeloginActions.xml'
);

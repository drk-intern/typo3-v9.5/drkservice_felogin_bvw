Frontend Login für DRK-Intern per SOAP.

Das Plugin besteht aus zwei Plugin Typen: Login Formular und das Plugin für die Ticketverarbeitung.

Konfiguration im Typo3 BE:
- eine Seite Login erstellen mit neuen Contentelement: Plugin "DRK Service BVW FE Login" -> Plugin Type "Login Formular"
- (Pflicht) Seite ID, wo das Action Login liegt in das Plugin Type "Login Formular" eintragen

- zweite Seite erstellen mit neuen Contentelement: Plugin "DRK Service BVW FE Login" -> Plugin Type "Login"
- (Pflicht) WSDL eintragen
- (Pflicht) Nutzerdaten Storage PID eintragen
- (Optional) Redirect Seiten ID eintragen
<?php
defined('TYPO3') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'drkservice_felogin_bvw',
            'Felogin',
            [
                \Oranto\DrkserviceFeloginBvw\Controller\FeloginController::class => 'loginForm, login'
            ],
            // non-cacheable actions
            [
                \Oranto\DrkserviceFeloginBvw\Controller\FeloginController::class => 'loginForm, login'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    felogin {
                        iconIdentifier = drkservice_felogin_bvw-plugin-felogin
                        title = LLL:EXT:drkservice_felogin_bvw/Resources/Private/Language/locallang_db.xlf:tx_drkservice_felogin_bvw_felogin.name
                        description = LLL:EXT:drkservice_felogin_bvw/Resources/Private/Language/locallang_db.xlf:tx_drkservice_felogin_bvw_felogin.description
                        tt_content_defValues {
                            CType = list
                            list_type = drkservicefeloginbvw_felogin
                        }
                    }
                }
                show = *
            }
       }'
    );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

			$iconRegistry->registerIcon(
				'drkservice_felogin_bvw-plugin-felogin',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:drkservice_felogin_bvw/Resources/Public/Icons/user_plugin_felogin.svg']
			);

    }
);

/***************
 * Die vom Cronjob ausgeführten Scheduler-Aufgaben.
 */
// Register the cacheTask
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['Oranto\DrkserviceFeloginBvw\Task\FeuserTask'] = array(
	'extension' => 'drkservice_felogin_bvw',
	'title' => 'LLL:EXT:drkservice_felogin_bvw/Resources/Private/Language/locallang_task.xlf:feuser.title',
	'description' => 'LLL:EXT:drkservice_felogin_bvw/Resources/Private/Language/locallang_task.xlf:feuser.description'
);
